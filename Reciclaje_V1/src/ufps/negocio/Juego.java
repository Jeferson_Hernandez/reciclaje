/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.negocio;

import java.applet.AudioClip;
import ufps.modelo.CanecaNoReciclable;
import ufps.modelo.Material;
import ufps.util.ArchivoLeerTexto;

/**
 *
 * @author Jeferson Hernandez
 */
public class Juego {

    /**
     * Enteros {nivel, puntaje, velocidad juego, xPos, residuosErroneo,
     * ResiduoExitoso, vida, bono}
     */
    private String url = "/ufps/util/";
    private AudioClip sonido;
    private CanecaNoReciclable canecaResiduos;
    private Material[] residuos;
    private int v[] = new int[8];

    public Juego() {
        cargarImagenes();
//        cargarSonido();
//        CargarPuntajesAnteriores();
//        cargarObjetivosDeNivel();
//        iniciarJuego();
    }

    private void cargarImagenes() {
        ArchivoLeerTexto a = new ArchivoLeerTexto("listado_residuos");
        String s[] = a.getDatos();
        this.residuos = new Material[s.length];
        for (int j = 0; j < this.residuos.length; j++) {
            this.residuos[j] = new Material(this.url + s[j]);
        }
    }

    private void iniciarJuego() {
        this.residuos = new Material[40];
        this.v[0] = 0;
        this.v[1] = 0;
        this.v[2] = 2;
        this.v[3] = 120;
        this.v[4] = 0;
        this.v[5] = 0;
        this.v[6] = 3;
        this.v[7] = 0;
//        this.on = false;
//        this.animacion = false;
//        this.animacionBono = false;
//        this.perdio = false;
//        VentanaNivel.pausa = true;
//        this.pivote = false;
        this.sonido = java.applet.Applet.newAudioClip(getClass().getResource(this.url + "gotcha.wav"));
        this.canecaResiduos = new CanecaNoReciclable();
    }

    public static void main(String[] args) {
        new Juego();
    }

}
