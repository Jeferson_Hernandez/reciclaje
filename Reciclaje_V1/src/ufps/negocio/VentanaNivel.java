/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.negocio;

import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import ufps.modelo.CanecaCarton;
import ufps.modelo.CanecaPlastico;
import ufps.modelo.CanecaVidrio;
import ufps.modelo.Hilo;
import ufps.modelo.Lienzo;

/**
 *
 * @author Jeferson Hernandez
 */
public class VentanaNivel extends JFrame {

    public static boolean pausa;
    private Lienzo lienzo;
    private Hilo hiloCreacionObjetos;

    public VentanaNivel() {
        this.lienzo = new Lienzo();
        this.hiloCreacionObjetos = new Hilo(lienzo);
        this.hiloCreacionObjetos.start();

        lienzo.setPreferredSize(new Dimension(400, 600));
        this.add(lienzo);
        this.pack();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {

                if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    lienzo.moverIzquierda();
                }
                if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    lienzo.moverDerecha();
                }
                if (e.getKeyCode() == KeyEvent.VK_R) {
                    lienzo.setMostrarRecord(true);
                }
                if (e.getKeyCode() == KeyEvent.VK_C) {
                    lienzo.setOn(true);
                }
                if (lienzo.getNivel() == 5) {
                    if (e.getKeyCode() == KeyEvent.VK_Q) {
                        lienzo.setCanecaResiduo(new CanecaPlastico());
                    }
                    if (e.getKeyCode() == KeyEvent.VK_W) {
                        lienzo.setCanecaResiduo(new CanecaCarton());
                    }
                    if (e.getKeyCode() == KeyEvent.VK_E) {
                        lienzo.setCanecaResiduo(new CanecaVidrio());
                    }

                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                    lienzo.moverIzquierda();
                }
                if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                    lienzo.moverDerecha();

                }
                if (e.getKeyCode() == KeyEvent.VK_C) {
                    lienzo.setOn(false);
                }
                if (e.getKeyCode() == KeyEvent.VK_R) {
                    lienzo.setMostrarRecord(false);
                }
            }
        }
        );

    }

    

}
