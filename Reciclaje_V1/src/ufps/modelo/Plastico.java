/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author jeferson
 */
public class Plastico extends Material {
    
    public Plastico() {
        super("Plastico");       
        super.imagen = getImagen();
    }

    private Image getImagen() {
        Image a = null;
        switch ((int) Math.floor(Math.random() * (1 - 4) + 4)) {
            case 1:
                a = new ImageIcon(getClass().getResource("/ufps/util/botella-agua.png")).getImage();
                break;

            case 2:
                a = new ImageIcon(getClass().getResource("/ufps/util/chips.png")).getImage();
                break;

            case 3:
                a = new ImageIcon(getClass().getResource("/ufps/util/plastic-cup.png")).getImage();
                break;
        }
        return a;
    }
}
