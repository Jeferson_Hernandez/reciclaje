/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author jeferson
 */
public class NoReciclabe extends Material {

    public NoReciclabe() {
        super("noReciclable");
        super.imagen = getImagen();
    }

    private Image getImagen() {
        Image a = null;
        switch ((int) (Math.random() * 5 + 1)) {
            case 1:
                a = new ImageIcon(getClass().getResource("/ufps/util/bombilla.png")).getImage();
                break;

            case 2:
                a = new ImageIcon(getClass().getResource("/ufps/util/florero.png")).getImage();
                break;

            case 3:
                a = new ImageIcon(getClass().getResource("/ufps/util/spray-paint.png")).getImage();
                break;
            case 4:
                a = new ImageIcon(getClass().getResource("/ufps/util/tasa-cafe.png")).getImage();
                break;
        }
        return a;
    }

}
