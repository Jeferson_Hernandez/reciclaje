/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author jeferson
 */
public class Vidrio extends Material{

    public Vidrio() {
        super("Vidrio");
        super.imagen = getImagen();
    }
    
        private Image getImagen() {
        Image a = null;
        switch ((int) Math.floor(Math.random() * (1 - 4) + 4)) {
            case 1:
                a = new ImageIcon(getClass().getResource("/ufps/util/water.png")).getImage();
                break;

            case 2:
                a = new ImageIcon(getClass().getResource("/ufps/util/wine.png")).getImage();
                break;

            case 3:
                a = new ImageIcon(getClass().getResource("/ufps/util/window.png")).getImage();
                break;
        }
        return a;
    }
    
    
    
}
