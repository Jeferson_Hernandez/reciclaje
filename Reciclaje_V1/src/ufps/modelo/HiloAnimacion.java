/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

import static ufps.negocio.VentanaNivel.pausa;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jeferson Hernandez
 */
public class HiloAnimacion extends Thread {

    private Lienzo lienzo;

    public HiloAnimacion(Lienzo lienzo) {
        this.lienzo = lienzo;
    }

    public void run() {
        while (pausa) {
               System.out.println();
               try {
           if(lienzo.getAnimacion()){
                                  Thread.sleep(70);
                   lienzo.setAnimacion();
           }
               } catch (InterruptedException ex) {
                   Logger.getLogger(HiloAnimacion.class.getName()).log(Level.SEVERE, null, ex);
               }
        }
    }
}
