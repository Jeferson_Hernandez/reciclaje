/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author jeferson
 */
public class Material {

    protected String nombre;
    protected Image imagen;
    private int yPosicion;
    private int xPosicion;
    private boolean on;

    public Material(String nombre) {
        this.nombre = nombre;
        this.imagen = new ImageIcon(getClass().getResource(nombre)).getImage();;
        this.yPosicion = 0;
        this.xPosicion = 0;
        this.on = false;
    }

    public int getyPosicion() {
        return yPosicion;
    }

    public void setyPosicion(int yPosicion) {
        this.yPosicion = yPosicion;
    }

    public int getxPosicion() {
        return xPosicion;
    }

    public void setxPosicion(int xPosicion) {
        this.xPosicion = xPosicion;
    }

    public boolean isOn() {
        return on;
    }

    public void setOn(boolean on) {
        this.on = on;
    }

    public String getNombre() {
        return nombre;
    }

}
