/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jeferson
 */
public class LeerDatos {

    private Jugador mejoresPuntajes[];

    public LeerDatos() throws ClassNotFoundException, IOException {
        mejoresPuntajes = new Jugador[3];
        getLectura();

    }

    /**
     * RETORNA TRUE SI EL PUNTAJE DEL JUGADOR ACTUAL ES MAYOR A ALGUNO DE LOS
     * PUNTAJES QUE ESTAN EN EL RECORD DEL JUEGO
     *
     * @param puntaje
     * @return
     */
    public boolean verificarRecord(int puntaje) {
        boolean a = false;
        for (Jugador mejoresPuntaje : mejoresPuntajes) {
            if (puntaje > mejoresPuntaje.getPuntos()) {
                a = true;
            }
        }
        return a;
    }

    /**
     * GUARDA A LOS JUGADORES EN UN ARCHIVO.OBJ
     *
     * @throws IOException
     */
    public void setEscritura() throws IOException {
        File f = new File("datos.obj");
        FileOutputStream fos = new FileOutputStream(f);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(mejoresPuntajes[0]);
        oos.writeObject(mejoresPuntajes[1]);
        oos.writeObject(mejoresPuntajes[2]);
        oos.close();

    }

    /**
     * INICIALIZA UN ARREGLO LOCAL DE JUGADORES CON JUGADORES GUARDADOS EN UN
     * ARCHIVO
     *
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public void getLectura() throws ClassNotFoundException, IOException {
        ObjectInputStream ois = null;
        try {
            File f = new File("datos.obj");
            FileInputStream fis = new FileInputStream(f);
            ois = new ObjectInputStream(fis);
            for (int i = 0; i < mejoresPuntajes.length; i++) {
                mejoresPuntajes[i] = (Jugador) ois.readObject();
            }
 ordenar();
        } catch (IOException e) {
        } finally {
            ois.close();
        }

    }

    private void ordenar() {
        Jugador mayor = mejoresPuntajes[0];
        for (int i = 0; i < mejoresPuntajes.length; i++) {
            for (int j = i + 1; j < mejoresPuntajes.length; j++) {
                if (mejoresPuntajes[i].getPuntos() < mejoresPuntajes[j].getPuntos()) {
                    mayor = mejoresPuntajes[i];
                    mejoresPuntajes[i] = mejoresPuntajes[j];
                    mejoresPuntajes[j] = mayor;
                }
            }
        }
    }

    /**
     * GUARDA A JUGADOR EN EL ARREGLO
     *
     * @param ganador
     */
    void setRecord(Jugador ganador) {
        try {
            Jugador mayor = mejoresPuntajes[0];
            for (int i = 0; i < mejoresPuntajes.length; i++) {
                for (int j = i + 1; j < mejoresPuntajes.length; j++) {
                    if (mejoresPuntajes[i].getPuntos() < mejoresPuntajes[j].getPuntos()) {
                        mayor = mejoresPuntajes[i];
                        mejoresPuntajes[i] = mejoresPuntajes[j];
                        mejoresPuntajes[j] = mayor;
                    }
                }
            }
            mejoresPuntajes[2] = ganador;
            setEscritura();

        } catch (IOException ex) {
            Logger.getLogger(LeerDatos.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

//    public static void main(String[] args) throws ClassNotFoundException, IOException {
//        LeerDatos a = new LeerDatos();
//        a.getLectura();
//        for (int i = 0; i < mejoresPuntajes.length; i++) {
//            System.out.println(mejoresPuntajes[i].getNombre() + "|" + mejoresPuntajes[i].getNivel() + "|" + mejoresPuntajes[i].getPuntos());
//
//        }
//    }
    public Jugador[] getArray() {
        return mejoresPuntajes;
    }
}
