/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

import java.awt.Toolkit;
import javax.swing.ImageIcon;

/**
 *
 * @author jeferson
 */
public class CanecaNoReciclable extends Caneca {

    public CanecaNoReciclable() {
        super("noReciclable");
        super.setCaneca(new ImageIcon(getClass().getResource("/ufps/util/caneca_noreciclable.png")).getImage());
    }

}
