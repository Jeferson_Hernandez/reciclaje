package ufps.modelo;

import ufps.negocio.VentanaNivel;
import java.applet.AudioClip;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Lienzo extends Canvas {

    private final HiloAnimacion hiloAnimacion;
    private final HiloBono hiloBono;

    private int bono;
    private int nivel;
    private int puntaje;
    private int residuosExitosos;
    private int residuosErroneos;
    private int velocidadDelJuego;
    private int xPosicion;
    private boolean on;
    private boolean animacion;
    private boolean animacionBono;
    private boolean pivote;
    private Image offImage;
    private Caneca canecaResiduos;
    private Material residuos[];
    private AudioClip sonido;
    private Graphics offGraphics;
    private int vida;
    private boolean perdio = false;
    private boolean mostrarRecord;

    public Lienzo() {
        iniciarJuego();
        this.hiloBono = new HiloBono(this);
        this.hiloAnimacion = new HiloAnimacion(this);
        hiloAnimacion.start();
        hiloBono.start();
    }

    @Override
    public void update(Graphics g) {
        try {
            if (offGraphics == null) {
                offImage = createImage(400, 600);
                offGraphics = offImage.getGraphics();
            }
            offGraphics.setColor(getBackground());
            offGraphics.fillRect(0, 0, 400, 600);
            offGraphics.setColor(Color.black);
            offGraphics.drawImage(new ImageIcon(getClass().getResource("/ufps/util/background.png")).getImage(), 0, 0, this);
            caida();
            Thread.sleep(8);
            jugar();
            mostrarAnimacionBono();
            pintarVidas();
            if (animacion) {
                offGraphics.setColor(Color.green);
                offGraphics.drawString("FELICITACIONES  ", 150, 400);
            }
            offGraphics.setColor(Color.CYAN);
            offGraphics.drawString("Puntos:                      Nivel:", 10, 10);
            offGraphics.setColor(Color.RED);
            offGraphics.drawString(+this.puntaje + "                           " + this.nivel, 70, 10);
            if (on) {
                mostrarTabla();
            }
            if(mostrarRecord){
                try {
                    mostrarRecords();
                } catch (ClassNotFoundException | IOException ex) {
                    Logger.getLogger(Lienzo.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
            if (animacionBono) {
                mostrarAnimacionBono();
            }

            offGraphics.drawImage(canecaResiduos.caneca, xPosicion, 475, this);
            g.drawImage(offImage, 0, 0, this);
        } catch (InterruptedException ex) {
            Logger.getLogger(Lienzo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * MUESTRA LA ANIMACION DE BONO TRAS VERIFICAR QUE SE HA OBTENIDO UNO
     */
    private void mostrarAnimacionBono() {
        if (bono == 5) {
            animacionBono = true;
        }
        if (animacionBono) {
            offGraphics.setColor(Color.YELLOW);
            offGraphics.drawLine(300, 460, 100, 460);
            offGraphics.drawString("BONO + 100 ", 165, 475);
            offGraphics.drawLine(300, 480, 100, 480);
        }
    }

    /**
     * MUESTRA UNA ANIMACION QUE INDICA QUE EL NIVEL HA SIDO SUPERADO Y
     * DESAPARECE LOS RESIDUOS QUE ESTABAN CALLENDO ASIGNA UNA NUEVA VELOCIDAD
     * AL NIVEL
     *
     * @param pivot
     * @param velocidad
     */
    private void iniciarNuevoNivel(boolean pivot, int velocidad) {
        animacion = true;
        for (Material residuo : residuos) {
            residuo.setOn(false);
        }
        pivote = pivot;
        nivel++;
        velocidadDelJuego = velocidad;
    }

    /**
     * INDICA EL CAMBIO DE NIVEL, ASIGNA LA CANECA DE RECICLAJE DEL MISMO
     */
    private void jugar() {
        if (residuosExitosos == 30 && !pivote) {
            canecaResiduos = new CanecaPlastico();
            iniciarNuevoNivel(true, 2);
        }
        if (residuosExitosos == 60 && pivote) {
            canecaResiduos = new CanecaCarton();
            iniciarNuevoNivel(false, 2);
        }
        if (residuosExitosos == 90 && !pivote) {
            canecaResiduos = new CanecaVidrio();
            iniciarNuevoNivel(true, 2);
        }
        if (residuosExitosos == 120 && pivote) {
            iniciarNuevoNivel(false, 2);
        }
        if (residuosExitosos == 150 && !pivote) {
            iniciarNuevoNivel(true, 2);
        }

    }

    /**
     * RETORNA TRUE SI EL RESIDUO QUE ENTRO EN LA CANECA ES EL QUE PERMITE ESE
     * NIVEL.
     *
     * @param residuosPermitidos
     * @param nivel
     * @return
     */
    private boolean objetivoDelNivel(String residuosPermitidos, int nivel) {
        boolean objetivo = false;
        if (nivel == 1) {
            if (!canecaResiduos.nombre.equals(residuosPermitidos)) {
                objetivo = true;
            }
        }
        if (nivel == 2) {
            if (canecaResiduos.nombre.equals(residuosPermitidos)) {
                objetivo = true;
            }
        }
        if (nivel == 3) {
            if (canecaResiduos.nombre.equals(residuosPermitidos)) {
                objetivo = true;
            }
        }
        if (nivel == 4) {
            if (canecaResiduos.nombre.equals(residuosPermitidos)) {
                objetivo = true;
            }

        }
        if (nivel == 5) {
            if (canecaResiduos.nombre.equals(residuosPermitidos)) {
                objetivo = true;
            }
        }

        return objetivo;

    }

    /**
     * MODIFICA LA POSOSICION EN EL EJE Y DE LOS RESIDUOS DANDELE EL EFECTO DE
     * CAIDA, VERIFICA SI EL OBJETO ENTRO EN LA CANECA O DE LO CONTRARIO LO
     * DESAPARECE AL SALIR DE LA VENTANA.
     *
     * SI EL RESIDUO ENTRO EN LA CANECA ENTONCES AGREGA EL PUNTAJE RESPECTIVO Y
     * EMITE EL SONIDO INDICANDO QUE FUE SATISFACTORIO
     */
    public void caida() {
        for (Material residuo : this.residuos) {
            if (residuo.isOn() && animacionBono == false) {
                residuo.setyPosicion(residuo.getyPosicion() + this.velocidadDelJuego);
                offGraphics.drawImage(residuo.imagen, residuo.getxPosicion(), residuo.getyPosicion(), this);
                if (areaCaneca(residuo)) {
                    residuo.setOn(false);
                    if (objetivoDelNivel(residuo.nombre, nivel)) {
                        this.puntaje = puntaje + 10;
                        residuosExitosos++;
                        bono++;
                        sonido.play();
                    } else {
                        vida--;
                        residuosErroneos++;
                        if (residuosErroneos == 3) {
                            perdio = true;
                        }
                    }
                }
                if (residuo.getyPosicion() > 600) {
                    residuo.setOn(false);
                }
            }
        }
        repaint();
    }

    /**
     * INICIA LOS ATRIBUTOS DEL JUEGO POR DEFECTO
     */
    private void iniciarJuego() {
        this.nivel = 1;
        this.residuos = new Material[60];
        puntaje = 0;
        this.iniciarVector();
        this.velocidadDelJuego = 2;
        this.xPosicion = 120;
        this.residuosErroneos = 0;
        this.residuosExitosos = 0;
        this.vida = 3;
        this.on = false;
        this.animacion = false;
        this.animacionBono = false;
        this.perdio = false;
        VentanaNivel.pausa = true;
        this.pivote = false;
        this.bono = 0;
        this.sonido = java.applet.Applet.newAudioClip(getClass().getResource("/ufps/util/gotcha.wav"));
        this.canecaResiduos = new CanecaNoReciclable();
    }

    @Override
    public void paint(Graphics g) {
        update(g);
    }

    /**
     * MUEVE LA CANECA DE BASURA HACIA LA DERECHA
     */
    public void moverIzquierda() {
        if (this.xPosicion >= 10) {
            this.xPosicion = xPosicion - 30;
        }
    }

    /**
     * MUEVE LA CANECA DE BASURA HACIA LA IZQUIERDA
     */
    public void moverDerecha() {
        if (this.xPosicion < 250) {
            this.xPosicion = xPosicion + 30;
        }
    }

    /**
     * CREA RESIDUOS AL AZAR
     */
    public void creacionDeObjetos() {
        int numAleatorio = (int) (Math.random() * ((this.residuos.length - 1) - 0 + 1) + 0);
        if (this.residuos[numAleatorio].isOn() == false) {
            this.residuos[numAleatorio] = agregarNuevoResiduo();
            this.residuos[numAleatorio].setOn(true);
            this.residuos[numAleatorio].setyPosicion(0);
        }
    }

    /**
     * RETORNA TRUE SI UN RESIDUO ENTRA EN EL AREA DE LA CANECA DE RECICLAJE
     *
     * @param a
     * @return
     */
    private boolean areaCaneca(Material a) {
        return (a.getxPosicion() >= this.xPosicion && a.getxPosicion() <= this.xPosicion + 128 && a.getyPosicion() >= 450 && a.getyPosicion() <= 452);
    }

    private void iniciarVector() {
//        for (Material m : residuos) {
//            m=new Material("default");
//        }
    }

    /**
     * MUESTRA LA TABLA DE ESTADISTICAS DE LA PARTIDA
     */
    private void mostrarTabla() {
        int xPos = 0;
        int yPos = 0;
        offGraphics.setColor(Color.white);
        offGraphics.drawLine(390, 60, 5, 60);
        offGraphics.drawLine(200, 80, 5, 80);
        offGraphics.drawLine(200, 100, 5, 100);
        offGraphics.drawLine(200, 120, 5, 120);
        offGraphics.drawLine(390, 140, 5, 140);
        offGraphics.drawLine(5, 60, 5, 140);
        offGraphics.drawLine(100, 60, 100, 140);
        offGraphics.drawLine(200, 60, 200, 140);
        offGraphics.drawLine(390, 60, 390, 140);//x, inicio en y,y,fin en y

        offGraphics.drawString("Puntaje:                " + Integer.toString(puntaje), xPos + 10, yPos + 75);
        offGraphics.drawString("Exitosos:               " + Integer.toString(residuosExitosos), xPos + 10, yPos + 95);
        offGraphics.drawString("Erroneos:              " + Integer.toString(residuosErroneos), xPos + 10, yPos + 115);
        offGraphics.drawString("Nivel:                     " + Integer.toString(nivel), xPos + 10, yPos + 135);
        if (nivel == 1) {
                offGraphics.drawString("objetivo:", xPos + 205, yPos + 75);
                offGraphics.drawString("Atrapa todos los residuos", xPos + 205, yPos + 90);
                offGraphics.drawString("reciclables en la caneca", xPos + 205, yPos + 105);
                offGraphics.drawString("y obten puntos.", xPos + 205, yPos + 120);
                offGraphics.drawString("", xPos + 205, yPos + 135);

        }
        if (nivel == 2) {
            offGraphics.drawString("objetivo:", xPos + 205, yPos + 75);
            offGraphics.drawString("Atrapa los residuos", xPos + 205, yPos + 90);
            offGraphics.drawString("plasticos en la caneca", xPos + 205, yPos + 105);
            offGraphics.drawString("y obten puntos.", xPos + 205, yPos + 120);
            offGraphics.drawString("", xPos + 205, yPos + 135);
        }
        if (nivel == 3) {
            offGraphics.drawString("objetivo:", xPos + 205, yPos + 75);
            offGraphics.drawString("Atrapa los residuos de", xPos + 205, yPos + 90);
            offGraphics.drawString("papel o carton y acumula", xPos + 205, yPos + 105);
            offGraphics.drawString("5 exitos para obtener un", xPos + 205, yPos + 120);
            offGraphics.drawString("bono +100.", xPos + 205, yPos + 135);
        }
        if (nivel == 4) {
            offGraphics.drawString("objetivo:", xPos + 205, yPos + 75);
            offGraphics.drawString("Atrapa los residuos de ", xPos + 205, yPos + 90);
            offGraphics.drawString("vidro y acumula 5 exitos", xPos + 205, yPos + 105);
            offGraphics.drawString("para ganar bono +100.", xPos + 205, yPos + 120);
            offGraphics.drawString("", xPos + 205, yPos + 135);
        }
        if (nivel == 5) {
            offGraphics.drawString("objetivo:", xPos + 205, yPos + 75);
            offGraphics.drawString("Atrapa los residuos en ", xPos + 205, yPos + 90);
            offGraphics.drawString("su respectiva caneca", xPos + 205, yPos + 105);
            offGraphics.drawString("cambia de caneca con ", xPos + 205, yPos + 120);
            offGraphics.drawString("'Q' 'W' 'E' exitos.", xPos + 205, yPos + 135);
        }

    }

    private void mostrarRecords() throws ClassNotFoundException, IOException {
         LeerDatos a = new LeerDatos();
                Jugador varios[] = a.getArray();
                offGraphics.setColor(Color.RED);
                offGraphics.drawString("Nombre    |    Nivel     |   Puntos ", 100, 300);
                offGraphics.setColor(Color.WHITE);
                offGraphics.drawString(varios[0].getNombre() + "         " + varios[0].getNivel() + "         " + varios[0].getPuntos(), 100, 320);
                offGraphics.drawString(varios[1].getNombre() + "         " + varios[1].getNivel() + "         " + varios[1].getPuntos(), 100, 340);
                offGraphics.drawString(varios[2].getNombre() + "         " + varios[2].getNivel() + "         " + varios[2].getPuntos(), 100, 360);
            
    }

    /**
     * PINTA EN LA PANTALLA LA CANTIDAD DE VIDAS DEL JUGADOR
     */
    public void pintarVidas() {
        switch (vida) {
            case 3:
                offGraphics.drawImage(new ImageIcon(getClass().getResource("/ufps/util/corazon_vida.png")).getImage(), 380, 5, this);
                offGraphics.drawImage(new ImageIcon(getClass().getResource("/ufps/util/corazon_vida.png")).getImage(), 360, 5, this);
                offGraphics.drawImage(new ImageIcon(getClass().getResource("/ufps/util/corazon_vida.png")).getImage(), 340, 5, this);
                break;
            case 2:
                offGraphics.drawImage(new ImageIcon(getClass().getResource("/ufps/util/corazon_vida.png")).getImage(), 380, 5, this);
                offGraphics.drawImage(new ImageIcon(getClass().getResource("/ufps/util/corazon_vida.png")).getImage(), 360, 5, this);
                break;
            case 1:
                offGraphics.drawImage(new ImageIcon(getClass().getResource("/ufps/util/corazon_vida.png")).getImage(), 380, 5, this);
                break;
            default:
                break;
        }
    }

    /**
     * AGREGA UN RESIDUO AL AZAR DE 1 A 4 E INDICA SU POSICION EN EL EJE X DONDE
     * APARECERA EL RESIDUO TRAS SU CREACION
     *
     * @return
     */
    private Material agregarNuevoResiduo() {
        Material a = new Material("Default");

        switch ((int) (Math.random() * 4 + 1)) {
            case 1:
                a = new Plastico();
                break;

            case 2:
                a = new Carton();
                break;

            case 3:
                a = new Vidrio();
                break;

            case 4:
                a = new NoReciclabe();
        }
        switch ((int) (Math.random() * 6 + 1)) {
            case 1:
                a.setxPosicion(10);
                break;

            case 2:
                a.setxPosicion(70);
                break;
            case 3:
                a.setxPosicion(130);
                break;
            case 4:
                a.setxPosicion(190);
                break;
            case 5:
                a.setxPosicion(250);
                break;
            case 6:
                a.setxPosicion(360);
                break;
        }
        return a;
    }

    public int getBono() {
        return bono;
    }

    public void setPuntaje() {
        this.puntaje = puntaje + 100;
    }

    public boolean isAnimacionBono() {
        return animacionBono;
    }

    public int getNivel() {
        return nivel;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setOn(boolean a) {
        this.on = a;
    }

    boolean getAnimacion() {
        return animacion;
    }

    void setAnimacion() {
        animacion = false;
    }

    int getVelocidad() {
        return velocidadDelJuego;
    }

    public void setBono() {
        this.bono = 0;
    }

    public void setAnimacionBono(boolean animacionBono) {
        this.animacionBono = animacionBono;
    }

    public void setCanecaResiduo(Caneca caneca) {
        canecaResiduos = caneca;
    }

    public void perdioNivel() {
        try {
            LeerDatos leer = new LeerDatos();
            if (leer.verificarRecord(puntaje)) {
                leer.setRecord(new Jugador(JOptionPane.showInputDialog(null,
                        "Nombre", "Guardar Datos", JOptionPane.INFORMATION_MESSAGE), Integer.toString(nivel), puntaje));
            }
            iniciarJuego();
            Hilo.play = true;
        } catch (ClassNotFoundException | IOException ex) {
            Logger.getLogger(Lienzo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean isPerdio() {
        return perdio;
    }

    public boolean isMostrarRecord() {
        return mostrarRecord;
    }

    public void setMostrarRecord(boolean mostrarRecord) {
        this.mostrarRecord = mostrarRecord;
    }

}
