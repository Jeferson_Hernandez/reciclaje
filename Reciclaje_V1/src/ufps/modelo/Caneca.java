/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

import java.awt.Image;

/**
 *
 * @author Jeferson Hernandez
 */
public class Caneca {

    protected String nombre;
    protected Image caneca;

    public Caneca(String nombre) {
        this.nombre = nombre;
    }

    protected void setCaneca(Image caneca) {
        this.caneca = caneca;
    }

}
