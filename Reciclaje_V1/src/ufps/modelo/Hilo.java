/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jeferson Hernandez
 */
public class Hilo extends Thread {

    private Lienzo juego;
    private int velicidadDeCreacion;
    public static boolean play = true;
    
    public Hilo(Lienzo juego) {
        this.juego = juego;
    }

    public void detener(){
        this.play = false;
    }
    
    @Override
    public void run() {
        while (play) {
            juego.creacionDeObjetos();
            if (juego.getNivel() == 1 || juego.getNivel() == 2 || juego.getNivel() == 3 || juego.getNivel() == 4) {
                velicidadDeCreacion = 900;
            }
            if(this.juego.isPerdio()){
                this.detener();
                this.juego.perdioNivel();
            }
            try {
                Thread.sleep(velicidadDeCreacion);
            } catch (InterruptedException ex) {
            	System.err.println(ex.getMessage());
            }
        }
    }
}
