/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

import static ufps.negocio.VentanaNivel.pausa;
import java.util.logging.Level;
import java.util.logging.Logger;
import static ufps.modelo.Hilo.play;

/**
 *
 * // * @author jeferson
 */
public class HiloBono extends Thread {

    private Lienzo lienzo;

    public HiloBono(Lienzo lienzo) {
        this.lienzo = lienzo;
    }

    @Override
    public void run() {
        while (pausa) {
            if (!play) {
                play = true;
            }
            if (lienzo.isAnimacionBono()) {
                try {
                    Thread.sleep(10);
                    lienzo.setBono();

                } catch (InterruptedException ex) {
                    Logger.getLogger(HiloBono.class.getName()).log(Level.SEVERE, null, ex);
                }
                lienzo.setPuntaje();
            }
            lienzo.setAnimacionBono(false);
        }

    }
}
