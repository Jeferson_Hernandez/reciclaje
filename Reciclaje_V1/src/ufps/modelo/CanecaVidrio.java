/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

import javax.swing.ImageIcon;

/**
 *
 * @author jeferson
 */
public class CanecaVidrio extends Caneca {

    public CanecaVidrio() {
        super("Vidrio");
        super.setCaneca(new ImageIcon(getClass().getResource("/recursos/caneca_vidrio.png")).getImage());
    }

}