/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

import java.io.Serializable;

/**
 *
 * @author jeferson
 */
public class Jugador implements Serializable {

    private String nombre;
    private String nivel;
    private int puntos;

    public Jugador(String nombre, String nivel, int puntos) {
        super();
        this.nombre = nombre;
        this.nivel = nivel;
        this.puntos = puntos;
    }

    public String getNombre() {
        return nombre;
    }

    public String getNivel() {
        return nivel;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }
    

}
