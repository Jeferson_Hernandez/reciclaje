/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

import java.awt.Image;
import java.awt.Toolkit;

/**
 *
 * @author jeferson
 */
public class Carton extends Material{

    public Carton() {
        super("Carton");
        super.imagen = getImagen();
    }
    
        private Image getImagen() {
        Image a = null;
        Toolkit tookit = Toolkit.getDefaultToolkit();
        switch ((int) Math.floor(Math.random() * (1 - 4) + 4)) {
            case 1:
                a = tookit.getImage("/home/jeferson/NetBeansProjects/Reciclaje/recursos/box.png");
                break;

            case 2:
                a = tookit.getImage("/home/jeferson/NetBeansProjects/Reciclaje/recursos/folder.png");
                break;

            case 3:
                a = tookit.getImage("/home/jeferson/NetBeansProjects/Reciclaje/recursos/notebook.png");
                break;
        }
        return a;
    }
    
    
}
