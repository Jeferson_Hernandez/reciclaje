/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

import javax.swing.ImageIcon;

/**
 *
 * @author jeferson
 */
public class CanecaPlastico extends Caneca {

    public CanecaPlastico() {
        super("Plastico");
        super.setCaneca(new ImageIcon(getClass().getResource("/ufps/util/caneca_plastico.png")).getImage());
    }

}
